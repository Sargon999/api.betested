export const constants = {
  IGNORE_VALUE: 'WAS IGNORED',
  TOKEN_REGEX: /^([a-zA-Z0-9_=]+)\.([a-zA-Z0-9_=]+)\.([a-zA-Z0-9_\-\+\/=]*)/,
  BODY_METHODS: ['POST', 'PUT'],
};
