export const toCapitalizeFirstLetter = (text: string): string =>
  text[0].toUpperCase() + text.substring(1);
