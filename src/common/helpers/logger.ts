import * as fs from 'fs';
import * as moment from 'moment';

export const logger = (prefix: string) => {
  return (value: string) => {
    fs.appendFile(`logs/${prefix}_${moment()}.txt`, value, (err) => err);
  };
};
