export enum TestCriteries {
  spped_of_test = 'json_response',
  presence_of_errors = 'presence_of_errors',
  correctness_of_response = 'correctness_of_response',
}