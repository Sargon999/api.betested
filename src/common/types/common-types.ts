export interface SelectorParams {
  value: string;
  label: string;
}
