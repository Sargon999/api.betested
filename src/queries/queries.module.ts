import { forwardRef, Module } from '@nestjs/common';
import { QueriesController } from './queries.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Query, QueriesSchema } from './schemas/queries.schema';
import { QueriesService } from './queries.service';
import { TestsModule } from '../tests/tests.module';
import { LoginQueriesSchema, LoginQuery } from './schemas/login-queries.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Query.name, schema: QueriesSchema },
      { name: LoginQuery.name, schema: LoginQueriesSchema },
    ]),
    forwardRef(() => TestsModule),
  ],
  controllers: [QueriesController],
  providers: [QueriesService],
  exports: [QueriesService],
})

export class QueriesModule {}
