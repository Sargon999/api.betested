import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

export type LoginQueryDocument = LoginQuery & Document;

@Schema()
export class LoginQuery {
  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Boolean,
    required: true,
    default: false,
  })
  isBearerAuth: boolean;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    default: '',
  })
  cookieTokenKey: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
    default: '',
  })
  tokenKey: string;
}

export const LoginQueriesSchema = SchemaFactory.createForClass(LoginQuery);
