import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { TestDocument } from '../../tests/test.schema';
import { LoginQueryDocument } from './login-queries.schema';
import { UserDocument } from '../../users/users.schema';

export type QueryDocument = Query & Document;

@Schema()
export class Query {
  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Number,
    required: true,
  })
  statusCode: number;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  statusMessage: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Mixed,
    required: true,
  })
  response: any;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Mixed,
    required: true,
  })
  requestJson: any;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Number,
    required: true,
  })
  speed: number;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  httpMethod: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  reqUrl: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Date,
    required: true,
    default: Date.now,
  })
  createdAt: number;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
  })
  headers: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Test',
    required: true,
  })
  test: TestDocument;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'LoginQuery',
    default: null,
  })
  loginQuery: LoginQueryDocument;
}

export const QueriesSchema = SchemaFactory.createForClass(Query);
