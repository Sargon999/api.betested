import { TestDocument } from '../../tests/test.schema';

export interface CreateQueryDto {
  statusMessage: string;
  statusCode: number;
  response: any;
  requestJson: any;
  speed: number;
  httpMethod: string;
  reqUrl: string;
  headers: string;
  test: TestDocument;
}