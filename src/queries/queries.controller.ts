import { Controller } from '@nestjs/common';
import { QueriesService } from './queries.service';
import { ApiTags } from '@nestjs/swagger';
@ApiTags('Queries')
@Controller('queries')
export class QueriesController {
  constructor(private queryService: QueriesService) {}
}
