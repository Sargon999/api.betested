import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Query, QueryDocument } from './schemas/queries.schema';
import { Model } from 'mongoose';
import { CreateQueryDto } from './dtos/create-query.dto';
import { TestsService } from '../tests/tests.service';
import { TestTypes } from '../tests/types/test.types';
import { constants } from '../common/constants/constants';
import { LoginQuery, LoginQueryDocument } from './schemas/login-queries.schema';
import { LoginTypes } from './types/login.types';

@Injectable()
export class QueriesService {
  constructor(
    @InjectModel(Query.name) private queryModel: Model<QueryDocument>,
    @InjectModel(LoginQuery.name)
    private loginQueryModel: Model<LoginQueryDocument>,
    private testsService: TestsService,
  ) {
  }

  async getQuery(queryId: string | Query): Promise<QueryDocument> {
    const query = await this.queryModel.findById(queryId);
    if (query?.loginQuery) {
      const loginQuery = await this.getLoginQuery(query.loginQuery);
      Object.assign(query, { loginQuery });
    }

    return query;
  }

  async getLoginQuery(loginQueryId: string | LoginQuery): Promise<LoginQueryDocument> {
    return this.loginQueryModel.findById(loginQueryId);
  }

  async create(params: CreateQueryDto): Promise<QueryDocument> {
    const { test } = params;

    if (test.type === TestTypes.login) {
      if (this.checkForLogin(params) && !test.queries.length) {
        return this.saveQuery(params);
      }
      if (test.queries.length === 1) {
        return this.updateLoginQueryBySecondQuery(
          test.queries[0],
          params,
        );
      }
    } else {
      return this.saveQuery(params);
    }
  }

  private async saveQuery(params: CreateQueryDto): Promise<QueryDocument> {
    const query = new this.queryModel(params);
    await query.save();
    await this.testsService.assignQuery(params.test._id, query);
    return query;
  }

  private async updateLoginQueryBySecondQuery(
    query: QueryDocument,
    secondQuery,
  ): Promise<QueryDocument> {
    const prepareQuery = await this.getQuery(query._id);
    if (!prepareQuery.loginQuery) {
      const [type, tokenKey] = this.getTokenInformation(
        JSON.parse(prepareQuery.response),
        JSON.parse(secondQuery.headers),
      );
      const loginQuery = new this.loginQueryModel({
        isBearerAuth: type === LoginTypes.bearer,
        tokenKey,
      });
      await loginQuery.save();
      Object.assign(prepareQuery, { loginQuery });
    }
    return prepareQuery.save();
  }

  private checkForLogin(params: CreateQueryDto): boolean {
    if (
      constants.BODY_METHODS.includes(params.httpMethod) &&
      params.httpMethod !== ''
    ) {
      const prepareParams = JSON.parse(params.response);
      let isHaveTokenParam: boolean;

      for (const param in prepareParams) {
        if (prepareParams[param].match(constants.TOKEN_REGEX)) {
          isHaveTokenParam = true;
          break;
        }
      }

      return isHaveTokenParam;
    }
    return false;
  }

  private getTokenInformation(response: any, headers: any): Array<string> {
    const bearerToken = headers.authorization.split(' ')[1];
    let type = '';
    let tokenKey = '';

    if (bearerToken && bearerToken.match(constants.TOKEN_REGEX)) {
      type = LoginTypes.bearer;
      for (const key in response) {
        if (response[key] === bearerToken) tokenKey = key;
      }
    }

    return [type, tokenKey];
  }
}
