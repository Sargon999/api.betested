import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './users.schema';
import { CreateUserDto } from './dtos/create-user.dto';
import { UserRoles } from './types/roles.enum';
import { OrganisationsService } from '../organisations/organisations.service';
import { OrganisationDocument } from '../organisations/organisations.schema';
import { Request } from 'express';
import { TokenService } from '../token/token.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @Inject(forwardRef(() => OrganisationsService))
    private organisationService: OrganisationsService,
    private tokenService: TokenService,
  ) {
  }

  async createUser(params: CreateUserDto): Promise<UserDocument> {
    let organisation: OrganisationDocument;

    if (params.userRole === UserRoles.organisation_admin) {
      organisation = await this.organisationService.createOrganisation({
        name: params.organisationName,
      });
    }

    if (params.userRole === UserRoles.tester) {
      organisation = await this.organisationService
        .findOrganisation(params.organisationId);
    }

    const user = await new this.userModel({
      ...params,
      organisation,
    });

    await user.save();

    if (params.userRole === UserRoles.organisation_admin) {
      await this.organisationService
        .updateOrganisation(organisation, { creator: user });
    }

    return user;
  }

  async getCurrentUser(req: Request): Promise<UserDocument> {
    const authHeader = req.headers.authorization;
    const token = authHeader?.split(' ')[1];
    const userData = this.tokenService.validateAccessToken(token);
    return await this.getUserBySocialMediaId(userData.socialMediaUserId);
  }

  async getUserBySocialMediaId(id: string): Promise<UserDocument> {
    return this.userModel.findOne({ socialMediaUserId: id });
  }

  async getUserById(id: User): Promise<UserDocument> {
    return this.userModel.findById(id);
  }
}