import { SocialMedias } from '../../auth/types/social-medias.enum';
import { UserRoles } from '../types/roles.enum';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  socialMediaUserId: string;

  @IsNotEmpty()
  @IsEmail()
  @IsString()
  email: string;

  @IsNotEmpty()
  @IsString()
  firstName: string;

  @IsString()
  lastName: string;

  @IsNotEmpty()
  @IsString()
  socialMedia: SocialMedias;

  @IsString()
  organisationName?: string;

  @IsString()
  organisationId?: string;

  @IsNotEmpty()
  @IsString()
  userRole: UserRoles;
}