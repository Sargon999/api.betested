import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { SocialMedias } from '../auth/types/social-medias.enum';
import { Organisation } from '../organisations/organisations.schema';
import { UserRoles } from './types/roles.enum';

export type UserDocument = User & Document;

@Schema()
export class User {
  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  socialMediaUserId: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  email: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  firstName: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  lastName: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  socialMedia: SocialMedias;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Date,
    required: true,
    default: Date.now,
  })
  createdAt: number;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  userRole: UserRoles;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Organisation',
  })
  organisation: Organisation;
}

export const UsersSchema = SchemaFactory.createForClass(User);