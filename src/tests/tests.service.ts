import {
  forwardRef,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  InternalServerErrorException, NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Test, TestDocument } from './test.schema';
import { CreateTestDto } from './dtos/create-test.dto';
import { QueryDocument } from '../queries/schemas/queries.schema';
import { QueriesService } from '../queries/queries.service';
import { TestHistoryService } from '../test-history/test-history.service';
import { Request } from 'express';
import { ProjectsService } from '../projects/projects.service';
import { UsersService } from '../users/users.service';
import { ResponseTestDto } from './dtos/response-test.dto';
import { ResponseTestPreviewDto } from './dtos/response-test-preview.dto';
import { ResponseTestHistoryDto } from './dtos/response-test-history.dto';

@Injectable()
export class TestsService {
  constructor(
    @InjectModel(Test.name) private testModel: Model<TestDocument>,
    @Inject(forwardRef(() => QueriesService))
    private queryService: QueriesService,
    private projectService: ProjectsService,
    private userService: UsersService,
    private testHistoryService: TestHistoryService,
  ) {
  }

  async getTest(id: string): Promise<TestDocument> {
    return this.testModel.findById(id);
  }

  async getAllTestsPreview(req: Request, type: string): Promise<Array<ResponseTestPreviewDto>> {
    const projects = await this.projectService.getProjects(req);

    const filter = {
      project: projects,
    };

    if (type) Object.assign(filter, { type });

    const tests: Array<TestDocument> = await this.testModel.find(filter);

    return tests.map(test => ({
      _id: test._id,
      name: test.name,
    }));
  }

  async getTestDetails(id: string): Promise<ResponseTestDto> {
    const test = await this.getTest(id);
    return this.serializeTest(test);
  }

  async createTest(params: CreateTestDto, req: Request): Promise<HttpException | TestDocument> {
    const project = await this.projectService.getProjectById(req.cookies?.selected_project_id);

    if (!project) {
      return new NotFoundException('Project not found!');
    }

    const user = await this.userService.getCurrentUser(req);

    if (!user) {
      return new NotFoundException('User not found!');
    }

    const test = new this.testModel({ ...params, project, creator: user });
    return test.save();
  }

  async assignQuery(id: string, query: QueryDocument): Promise<void> {
    const test = await this.testModel.findById(id);
    Object.assign(test, { queries: [...test.queries, query] });
    test.save();
  }

  async deleteTest(id: string): Promise<HttpException> {
    try {
      await this.testModel.deleteOne({ _id: id });
      return new HttpException('Test was deleted!', HttpStatus.OK);
    } catch (error) {
      throw new InternalServerErrorException();
    }
  }

  async runTest(
    id: string,
    loginQueryId?: string,
  ): Promise<ResponseTestHistoryDto> {
    const test = await this.getTest(id);
    const project = await this.projectService.getProjectById(test.project);

    if (test) {
      const resultOfTest = [];
      const testHistory = await this.testHistoryService.createTestHistory(test);
      let loginQuery = null;

      if (loginQueryId) {
        const loginTest = await this.getTest(loginQueryId);
        loginQuery = await this.queryService.getQuery(loginTest.queries[0]);
      }

      for (const query of test.queries) {
        const prepareQuery = await this.queryService.getQuery(query._id);

        const testHistoryItem = await this.testHistoryService.makeTestHistoryQuery({
          query: prepareQuery,
          loginQuery,
          test,
          targetUrl: project.targetProxyUrl,
          testHistory,
        });

        resultOfTest.push(testHistoryItem);
      }

      Object.assign(testHistory, { testQueries: resultOfTest });
      const prepareTestHistory = await testHistory.save();

      return this.testHistoryService.serializeTestHistory(prepareTestHistory);
    }
  }

  async serializeTest(test: TestDocument): Promise<ResponseTestDto> {
    const creator = await this.userService.getUserById(test.creator);
    const project = await this.projectService.getProjectById(test.project);
    const testHistory = await this.testHistoryService.getTestHistoryByTest(test);

    const prepareQueries: Array<QueryDocument> = [];

    for (const query of test.queries) {
      const preparedQuery = await this.queryService.getQuery(query);
      prepareQueries.push(preparedQuery);
    }

    return {
      _id: test._id,
      queries: prepareQueries,
      type: test.type,
      name: test.name,
      createdAt: test.createdAt,
      testHistory,
      creator: {
        _id: creator._id,
        name: `${creator.firstName} ${creator.lastName}`,
      },
      project: {
        _id: project._id,
        name: project.name,
      },
    };
  }
}
