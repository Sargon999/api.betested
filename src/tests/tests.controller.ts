import {
  Body,
  Controller,
  Delete,
  Get, HttpException,
  Param,
  Post,
  Put,
  Query,
  Req,
  Res, UseGuards,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TestsService } from './tests.service';
import { TestDocument } from './test.schema';
import { CreateTestDto } from './dtos/create-test.dto';
import { TestHistoryQueryDocument } from '../test-history/schemas/test-history-query.schema';
import { AuthGuard } from '../auth/auth.guard';
import { ResponseTestDto } from './dtos/response-test.dto';
import { ResponseTestPreviewDto } from './dtos/response-test-preview.dto';
import { TestHistoryDocument } from '../test-history/schemas/test-history.schema';
import { ResponseTestHistoryDto } from './dtos/response-test-history.dto';

@ApiTags('Tests')
@Controller('tests')
export class TestsController {
  constructor(
    private testService: TestsService,
  ) {
  }

  @Post('/')
  createTest(@Body() body: CreateTestDto, @Req() req): Promise<HttpException | TestDocument> {
    return this.testService.createTest(body, req);
  }

  @Get('/preview')
  getTestsPreview(
    @Req() req,
    @Query('type') type: string,
  ): Promise<Array<ResponseTestPreviewDto>> {
    return this.testService.getAllTestsPreview(req, type);
  }

  @Get('/:id')
  getTest(@Param() params): Promise<ResponseTestDto> {
    return this.testService.getTestDetails(params.id);
  }

  @Get('/:id/run')
  makeTest(
    @Param() params,
    @Query('loginquery_id') loginQueryId: string,
  ): Promise<ResponseTestHistoryDto> {
    return this.testService.runTest(params.id, loginQueryId);
  }

  @Delete('/:id')
  deleteTest(@Param() params) {
    return this.testService.deleteTest(params.id);
  }
}
