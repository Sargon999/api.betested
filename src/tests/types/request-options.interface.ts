export interface RequestOptions {
  method: string,
  mode: RequestMode,
  headers: {
    Accept: string,
    'Content-Type': string,
    Authorization: string,
  },
  body?: BodyInit,
}