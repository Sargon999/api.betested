import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { Query, QueryDocument } from '../queries/schemas/queries.schema';
import { ApiProperty } from '@nestjs/swagger';
import { ProjectDocument } from '../projects/projects.schema';
import { User } from '../users/users.schema';

export type TestDocument = Test & Document;

@Schema()
export class Test {
  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  name: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  type: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Date,
    required: true,
    default: Date.now,
  })
  createdAt: number;

  @ApiProperty()
  @Prop([
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Query',
    },
  ])
  queries: Array<QueryDocument>;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Project',
  })
  project: ProjectDocument;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  })
  creator: User;
}

export const TestSchema = SchemaFactory.createForClass(Test);
