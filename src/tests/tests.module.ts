import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Test, TestSchema } from './test.schema';
import { TestsService } from './tests.service';
import { TestsController } from './tests.controller';
import { QueriesModule } from '../queries/queries.module';
import { TestHistoryModule } from '../test-history/test-history.module';
import { ProjectsModule } from '../projects/projects.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Test.name, schema: TestSchema }]),
    forwardRef(() => QueriesModule),
    ProjectsModule,
    UsersModule,
    TestHistoryModule,
  ],
  controllers: [TestsController],
  providers: [TestsService],
  exports: [TestsService],
})
export class TestsModule {}
