import { TestHistoryQueryDocument } from '../../test-history/schemas/test-history-query.schema';

export interface ResponseTestHistoryDto {
  _id: string,
  createdAt: number,
  testQueries: Array<TestHistoryQueryDocument>,
}