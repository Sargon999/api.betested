import { QueryDocument } from '../../queries/schemas/queries.schema';
import { ResponseTestHistoryDto } from './response-test-history.dto';

export interface ResponseTestDto {
  _id: string;
  name: string;
  type: string;
  createdAt: number;
  queries: Array<QueryDocument>;
  testHistory: Array<ResponseTestHistoryDto>;
  project: {
    _id: string;
    name: string;
  };
  creator: {
    _id: string;
    name: string;
  };
}