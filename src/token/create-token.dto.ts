export interface CreateTokenDto {
  socialMediaUserId: string,
  email: string,
}