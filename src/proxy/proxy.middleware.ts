import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { createProxyMiddleware, responseInterceptor } from 'http-proxy-middleware';
import { QueriesService } from '../queries/queries.service';
import { ProjectsService } from '../projects/projects.service';
import { TestsService } from '../tests/tests.service';
import { RequestOptions } from '../tests/types/request-options.interface';
import fetch from 'node-fetch';

@Injectable()
export class ProxyMiddleware implements NestMiddleware {
  constructor(
    private projectService: ProjectsService,
    private queriesService: QueriesService,
    private testService: TestsService,
  ) {
  }

  rewriteFn(path) {
    return path.split('/').slice(3).join('/');
  };

  async use(request: Request, response: Response, next: NextFunction) {
    const btdProject = request.headers?.btdproject;
    const btdTest = request.headers?.btdtest;
    let currentProject = null;
    let currentTest = null;

    if (btdProject) currentProject = await this.projectService.getProjectById(btdProject as string);
    if (btdTest) currentTest = await this.testService.getTest(btdTest as string);

    let speedOfRequest = 0;
    let reqPath = '';
    let originalRequest = '';

    if (currentProject) {
      const proxy = createProxyMiddleware({
        target: currentProject.targetProxyUrl,
        pathRewrite: this.rewriteFn,
        selfHandleResponse: true,
        changeOrigin: true,
        secure: true,
        ws: true,

        onProxyReq: async function(proxyReq, req: any) {
          speedOfRequest = Date.now();
          reqPath = proxyReq.path;
          originalRequest = JSON.stringify(req.body);

          if (req.body) {
            const bodyData = JSON.stringify(req.body);
            proxyReq.setHeader('Content-Type', 'application/json');
            proxyReq.setHeader('Content-Length', Buffer.byteLength(bodyData));
            proxyReq.write(bodyData);
          }
        },

        onProxyRes: responseInterceptor(async (responseBuffer, proxyRes, req) => {
          const jsonResponse = responseBuffer.toString('utf8');

          if (!currentProject.ignorePaths.includes(reqPath) && currentTest) {
            const originUrl = new URL(currentProject.targetProxyUrl);
            const options: RequestOptions = {
              method: req.method,
              mode: 'cors',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: req.headers.authorization,
              },
            };

            if (originalRequest && req.method !== 'GET') options.body = originalRequest;

            const query = await fetch(
              `${originUrl.origin}${reqPath}`,
              options,
            );
            const responseJson = await query.json();

            await this.queriesService.create({
              statusCode: proxyRes.statusCode,
              statusMessage: proxyRes.statusMessage,
              requestJson: originalRequest,
              response: JSON.stringify(responseJson),
              speed: Date.now() - speedOfRequest,
              httpMethod: req.method,
              reqUrl: reqPath,
              headers: JSON.stringify(req.headers),
              test: currentTest,
            });
          }

          return jsonResponse;
        }),
      });

      return proxy(request, response, next);
    }
  }
}
