import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Query } from '@nestjs/common';
import { OrganisationsService } from './organisations.service';

@ApiTags('Organisations')
@Controller('organisations')
export class OrganisationsController {
  constructor(
    private organisationsService: OrganisationsService,
  ) {
  }

  @Get('/autocomplete')
  organisationsAutocomplete(
    @Query('search') search: string,
  ) {
    return this.organisationsService.organisationsAutocomplete(search);
  }
}