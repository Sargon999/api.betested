import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { User } from '../users/users.schema';

export type OrganisationDocument = Organisation & Document;

@Schema()
export class Organisation {
  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  name: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Date,
    required: true,
    default: Date.now,
  })
  createdAt: number;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  })
  creator: User;
}

export const OrganisationSchema = SchemaFactory.createForClass(Organisation);