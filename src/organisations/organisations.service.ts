import { forwardRef, HttpException, HttpStatus, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Organisation, OrganisationDocument } from './organisations.schema';
import { CreateOrganisationDto } from './dtos/create-organisation.dto';
import { SelectorParams } from '../common/types/common-types';
import { Request } from 'express';
import { TokenService } from '../token/token.service';
import { UsersService } from '../users/users.service';

@Injectable()
export class OrganisationsService {
  constructor(
    @InjectModel(Organisation.name) private organisationModel: Model<OrganisationDocument>,
    private tokenService: TokenService,
    @Inject(forwardRef(() => UsersService))
    private userService: UsersService,
  ) {
  }

  async organisationsAutocomplete(search: string): Promise<Array<SelectorParams>> {
    const organisations = await this.organisationModel.find(
      { name: { $regex: search, $options: 'i' } },
    );

    return organisations.map(organisation => {
      return {
        value: organisation._id,
        label: organisation.name,
      };
    });
  }

  async createOrganisation(params: CreateOrganisationDto): Promise<OrganisationDocument> {
    const user = new this.organisationModel(params);
    return user.save();
  }

  async findOrganisation(organisationId: string): Promise<OrganisationDocument> {
    return this.organisationModel.findById(organisationId);
  }

  async updateOrganisation(id, params): Promise<OrganisationDocument> {
    const organisation = await this.organisationModel.findById(id);
    Object.assign(organisation, params);
    return await organisation.save();
  }

  async getOrganisationByName(name): Promise<OrganisationDocument> {
    return this.organisationModel.findOne({ name });
  }

  async getCurrentOrganisation(req: Request): Promise<OrganisationDocument> {
    const user = await this.userService.getCurrentUser(req);

    if (!user) {
      throw new NotFoundException('User not found');
    }

    return this.organisationModel.findById(user.organisation);
  }
}