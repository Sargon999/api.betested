import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Organisation, OrganisationSchema } from './organisations.schema';
import { OrganisationsController } from './organisations.controller';
import { OrganisationsService } from './organisations.service';
import { TokenModule } from '../token/token.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Organisation.name, schema: OrganisationSchema }]),
    TokenModule,
    forwardRef(() => UsersModule),
  ],
  controllers: [OrganisationsController],
  providers: [OrganisationsService],
  exports: [OrganisationsService],
})

export class OrganisationsModule {
  
}