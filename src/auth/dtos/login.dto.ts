import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class LoginDto {
  @IsNotEmpty()
  @IsString()
  socialMediaUserId: string;

  @IsNotEmpty()
  @IsEmail()
  @IsString()
  email: string;
}