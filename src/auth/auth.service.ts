import {
  BadRequestException,
  ConflictException,
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { TokenService } from '../token/token.service';
import { ResponseAuthentication } from './types/response-authentication.interface';
import { CreateUserDto } from '../users/dtos/create-user.dto';
import { RedirectResponse } from '@nestjs/core/router/router-response-controller';
import { LoginDto } from './dtos/login.dto';
import { OrganisationsService } from '../organisations/organisations.service';
import { UserRoles } from '../users/types/roles.enum';

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private organisationService: OrganisationsService,
    private tokenService: TokenService,
  ) {
  }

  async authentication(req, res): Promise<RedirectResponse> {
    let params = req.user;
    console.log(req);
    if (!params) {
      throw new NotFoundException('No user from social media');
    }

    const user = await this.userService
      .getUserBySocialMediaId(params.socialMediaUserId);

    if (user) {
      return res
        .redirect(
          303,
          `https://betested.ml?params=${JSON.stringify({
            socialMediaUserId: params.socialMediaUserId,
            email: params.email,
          })}`,
        );
    } else {
      return res
        .redirect(
          303,
          `https://betested.ml/register?params=${JSON.stringify(params)}`,
        );
    }
  }

  async login(res, params: LoginDto): Promise<ResponseAuthentication> {
    const user = await this.userService
      .getUserBySocialMediaId(params.socialMediaUserId);

    if (!user) {
      throw new NotFoundException('User not found!');
    }

    const tokens = this.tokenService.generateTokens({
      socialMediaUserId: params.socialMediaUserId,
      email: params.email,
    });

    await this.setTokenToCookie(res, tokens.refreshToken);

    return { ...tokens, user };
  }

  async registration(res, params: CreateUserDto): Promise<ResponseAuthentication> {
    const preparedUser = await this.userService.getUserBySocialMediaId(params.socialMediaUserId);

    if (preparedUser) {
      throw new ConflictException('This user already exist!');
    }

    if (params.userRole === UserRoles.organisation_admin) {
      const preparedCompany = await this.organisationService.getOrganisationByName(params.organisationName);

      if (preparedCompany) {
        throw new ConflictException('An organization with the same name already exists!');
      }
    }

    const user = await this.userService.createUser(params);

    const tokens = this.tokenService.generateTokens({
      socialMediaUserId: user.socialMediaUserId,
      email: user.email,
    });

    await this.setTokenToCookie(res, tokens.refreshToken);

    return { ...tokens, user };
  }

  async refresh(req, res) {
    const { refreshToken } = req.cookies;

    if (!refreshToken) {
      throw new BadRequestException('User is not logged in');
    }

    const userData = this.tokenService.validateRefreshToken(refreshToken);

    if (!userData) {
      throw new BadRequestException('User is not logged in');
    }

    const user = await this.userService.getUserBySocialMediaId(userData.socialMediaUserId);

    const tokens = this.tokenService.generateTokens({
      socialMediaUserId: user.socialMediaUserId,
      email: user.email,
    });

    await this.setTokenToCookie(res, tokens.refreshToken);

    return { ...tokens };
  }

  async setTokenToCookie(res, refreshToken): Promise<void> {
    res.cookie('refreshToken', refreshToken, {
      expires: new Date(new Date().getTime() + 10 * 365 * 24 * 60 * 60),
      sameSite: 'strict',
      httpOnly: true,
      secure: true,
      maxAge: 10 * 365 * 24 * 60 * 60,
    });
  }

  async logout(res) {
    res.clearCookie('refreshToken');
    return new HttpException('You are logged out', HttpStatus.OK);
  }
}