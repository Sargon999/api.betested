import { User } from '../../users/users.schema';

export interface ResponseAuthentication {
  accessToken: string,
  refreshToken: string,
  user?: User,
}