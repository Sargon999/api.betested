import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { TokenService } from '../token/token.service';
import { UsersService } from '../users/users.service';
import { Reflector } from '@nestjs/core';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private tokenService: TokenService,
    private userService: UsersService,
    private reflector: Reflector,
  ) {
  }

  async canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest();
    const authHeader = req.headers.authorization;
    const bearer = authHeader?.split(' ')[0];
    const token = authHeader?.split(' ')[1];
    const allowedRoles = this.reflector
      .get<string>('roles', context.getHandler());

    if (bearer !== 'Bearer' || !token) {
      throw new UnauthorizedException('User is not logged in');
    }

    const userData = await this.tokenService.validateAccessToken(token);

    const user = await this.userService
      .getUserBySocialMediaId(userData.socialMediaUserId);

    if (!user) {
      throw new NotFoundException('User not found');
    }

    if (allowedRoles && !allowedRoles?.split(',').includes(user.userRole)) {
      throw new ForbiddenException(
        'This user does not have access to this action',
      );
    }

    return true;
  }
}