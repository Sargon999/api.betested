import { ApiTags } from '@nestjs/swagger';
import { Body, Controller, Get, HttpStatus, Post, Put, Req, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { ResponseAuthentication } from './types/response-authentication.interface';
import { CreateUserDto } from '../users/dtos/create-user.dto';
import { RedirectResponse } from '@nestjs/core/router/router-response-controller';
import { LoginDto } from './dtos/login.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {
  }

  @Get('/google')
  @UseGuards(AuthGuard('google'))
  async googleAuth(): Promise<any> {
    return HttpStatus.OK;
  }

  @Get('/google/redirect')
  @UseGuards(AuthGuard('google'))
  googleAuthRedirect(
    @Req() req,
    @Res({ passthrough: true }) res,
  ): Promise<RedirectResponse> {
    return this.authService.authentication(req, res);
  }

  @Get('/facebook')
  @UseGuards(AuthGuard('facebook'))
  async facebookLogin() {
    return HttpStatus.OK;
  }

  @Get('/facebook/redirect')
  @UseGuards(AuthGuard('facebook'))
  facebookAuthRedirect(
    @Req() req,
    @Res({ passthrough: true }) res,
  ): Promise<RedirectResponse> {
    return this.authService.authentication(req, res);
  }

  @Get('/linkedin')
  @UseGuards(AuthGuard('linkedin'))
  async linkedinLogin(): Promise<any> {
    return HttpStatus.OK;
  }

  @Get('/linkedin/redirect')
  @UseGuards(AuthGuard('linkedin'))
  linkedinAuthRedirect(
    @Req() req,
    @Res({ passthrough: true }) res,
  ): Promise<RedirectResponse> {
    return this.authService.authentication(req, res);
  }

  @Get('/refresh')
  refresh(
    @Req() req,
    @Res({ passthrough: true }) res,
  ): Promise<ResponseAuthentication> {
    return this.authService.refresh(req, res);
  }

  @Get('/logout')
  logout(@Res({ passthrough: true }) res) {
    return this.authService.logout(res);
  }


  @Post('/registration')
  registration(@Body() body: CreateUserDto, @Res({ passthrough: true }) res) {
    return this.authService.registration(res, body);
  }

  @Put('/login')
  login(@Body() body: LoginDto, @Res({ passthrough: true }) res) {
    return this.authService.login(res, body);
  }
}