import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { GoogleStrategy } from './google.strategy';
import { FacebookStrategy } from './facebook.strategy';
import { LinkedinStrategy } from './linkedin.strategy';
import { UsersModule } from '../users/users.module';
import { TokenModule } from '../token/token.module';
import { CorsMiddleware } from '../common/cors/cors.middleware';
import { OrganisationsModule } from '../organisations/organisations.module';

@Module({
  imports: [
    UsersModule,
    TokenModule,
    OrganisationsModule
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    GoogleStrategy,
    FacebookStrategy,
    LinkedinStrategy,
  ],
})

export class AuthModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(CorsMiddleware).forRoutes('');
  }
}