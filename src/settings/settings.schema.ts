import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

export type SettingsDocument = Settings & Document;

@Schema()
export class Settings {
  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
    default: '',
  })
  targetProxyUrl: string;

  @ApiProperty()
  @Prop([
    {
      type: mongoose.Schema.Types.String,
    },
  ])
  ignorePaths: Array<string>;
}

export const SettingsSchema = SchemaFactory.createForClass(Settings);
