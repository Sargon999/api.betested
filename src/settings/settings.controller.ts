import { ApiTags } from '@nestjs/swagger';
import { Body, Controller, Put } from '@nestjs/common';
import { SettingsService } from './settings.service';
import { Settings, SettingsDocument } from './settings.schema';

@ApiTags('Settings')
@Controller('settings')
export class SettingsController {
  constructor(private settingsService: SettingsService) {}
}
