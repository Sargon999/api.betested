import { IsArray, IsNotEmpty, IsString } from 'class-validator';

export class ProjectDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  targetProxyUrl: string;

  @IsString()
  description: string;

  @IsArray()
  ignorePaths?: Array<string>;

  @IsNotEmpty()
  @IsString()
  projectUrl: string;
}