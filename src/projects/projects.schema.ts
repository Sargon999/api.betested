import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { OrganisationDocument } from '../organisations/organisations.schema';

export type ProjectDocument = Project & Document;

@Schema()
export class Project {
  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  name: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
  })
  description: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Date,
    required: true,
    default: Date.now,
  })
  createdAt: number;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Organisation',
  })
  organisation: OrganisationDocument;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  targetProxyUrl: string;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.String,
    required: true,
  })
  projectUrl: string;

  @ApiProperty()
  @Prop([
    {
      type: mongoose.Schema.Types.String,
    },
  ])
  ignorePaths: Array<string>;
}

export const ProjectSchema = SchemaFactory.createForClass(Project);