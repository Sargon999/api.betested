import { SelectorParams } from '../../common/types/common-types';

export interface SelectorProjects extends SelectorParams {
  isSelected: boolean;
}