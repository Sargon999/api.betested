import { HttpException, HttpStatus, Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Project, ProjectDocument } from './projects.schema';
import { ProjectDto } from './project.dto';
import { Request, Response } from 'express';
import { OrganisationsService } from '../organisations/organisations.service';
import { SelectorProjects } from './types/selector-projects.type';


@Injectable()
export class ProjectsService {
  constructor(
    @InjectModel('Project') private projectsModel: Model<ProjectDocument>,
    private organisationServices: OrganisationsService,
  ) {
  }

  async createProject(params: ProjectDto, req: Request): Promise<ProjectDocument> {
    const organisation = await this.organisationServices
      .getCurrentOrganisation(req);

    return await this.projectsModel.create({
      ...params,
      organisation,
    });
  };

  async changeProject(id, params): Promise<ProjectDocument> {
    const project = await this.projectsModel.findById(id);
    Object.assign(project, params);
    return project.save();
  };

  async deleteProject(id: string): Promise<HttpException> {
    try {
      await this.projectsModel.deleteOne({ _id: id });
      return new HttpException('Project was deleted!', HttpStatus.OK);
    } catch (error) {
      throw new InternalServerErrorException();
    }
  };

  async getProjects(req: Request): Promise<Array<ProjectDocument>> {
    const organisation = await this.organisationServices
      .getCurrentOrganisation(req);

    return this.projectsModel.find({ organisation });
  };

  async getProject(id: string): Promise<ProjectDocument> {
    return this.projectsModel.findById(id);
  };

  async getProjectSelectorParams(req: Request): Promise<Array<SelectorProjects>> {
    const projects = await this.getProjects(req);
    const selectedProjectId = await this.getSelectedProjectId(req);
    return projects.map(project => {
      return {
        value: project._id,
        label: project.name,
        isSelected: project.id === selectedProjectId,
      };
    });
  };

  async getSelectedProjectId(req: Request): Promise<string> {
    return req.cookies?.selected_project_id;
  };

  async getProjectById(id: string | Project): Promise<ProjectDocument> {
    return this.projectsModel.findById(id);
  };

  setCurrentProject(id: string, res: Response) {
    try {
      res.cookie('selected_project_id', id, {
        expires: new Date(new Date().getTime() + 10 * 365 * 24 * 60 * 60),
        maxAge: 10 * 365 * 24 * 60 * 60,
        sameSite: 'none',
        httpOnly: true,
        secure: true,
      });
      return new HttpException('Project was selected!', HttpStatus.OK);
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
  };
}