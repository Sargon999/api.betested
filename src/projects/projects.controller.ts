import { ApiTags } from '@nestjs/swagger';
import {
  Body,
  Controller, Delete,
  Get, HttpException,
  Param,
  Post, Put,
  Req,
  Res,
  SetMetadata,
  UseGuards,
} from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { ProjectDto } from './project.dto';
import { AuthGuard } from '../auth/auth.guard';
import { UserRoles } from '../users/types/roles.enum';
import { ProjectDocument } from './projects.schema';
import { SelectorProjects } from './types/selector-projects.type';

@ApiTags('Projects')
@Controller('projects')
export class ProjectsController {
  constructor(private projectsService: ProjectsService) {
  }

  @Post()
  @UseGuards(AuthGuard)
  @SetMetadata('roles', UserRoles.organisation_admin)
  createProject(
    @Body() body: ProjectDto,
    @Res({ passthrough: true }) res,
    @Req() req,
  ): Promise<ProjectDocument> {
    return this.projectsService.createProject(body, req);
  }

  @Put('/:id')
  @UseGuards(AuthGuard)
  @SetMetadata('roles', UserRoles.organisation_admin)
  changeProject(
    @Body() body: ProjectDto,
    @Param() params,
  ): Promise<ProjectDocument> {
    return this.projectsService.changeProject(params.id, body);
  }

  @Delete('/:id')
  @UseGuards(AuthGuard)
  @SetMetadata('roles', UserRoles.organisation_admin)
  deleteProject(@Param() params): Promise<HttpException> {
    return this.projectsService.deleteProject(params.id);
  }

  @Get('/:id/details')
  @UseGuards(AuthGuard)
  getProject(@Param() params): Promise<ProjectDocument> {
    return this.projectsService.getProject(params.id);
  }

  @Get()
  @UseGuards(AuthGuard)
  getProjects(@Req() req): Promise<Array<ProjectDocument>> {
    return this.projectsService.getProjects(req);
  }

  @Get('/selector_params')
  @UseGuards(AuthGuard)
  getProjectSelectorParams(@Req() req): Promise<Array<SelectorProjects>> {
    return this.projectsService.getProjectSelectorParams(req);
  }

  @Get(':id/select')
  @UseGuards(AuthGuard)
  setCurrentProject(@Param() param, @Res({ passthrough: true }) res) {
    return this.projectsService.setCurrentProject(param.id, res);
  }
}