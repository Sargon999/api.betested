import { Global, Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { TokenModule } from './token/token.module';

@Global()
@Module({
  imports: [
    UsersModule,
    TokenModule,
  ],
  controllers: [],
  providers: [],
  exports: [
    UsersModule,
    TokenModule
  ]
})
export class GlobalModule {

}
