import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { QueriesModule } from './queries/queries.module';
import { TestsModule } from './tests/tests.module';
import { ProxyMiddleware } from './proxy/proxy.middleware';
import { SettingsModule } from './settings/settings.module';
import { TestHistoryModule } from './test-history/test-history.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { OrganisationsModule } from './organisations/organisations.module';
import { ProjectsModule } from './projects/projects.module';
import { GlobalModule } from './global.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseModule.forRoot(process.env.MONGO_CONFIG),
    SettingsModule,
    QueriesModule,
    TestsModule,
    TestHistoryModule,
    AuthModule,
    OrganisationsModule,
    ProjectsModule,
    GlobalModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ProxyMiddleware).forRoutes('proxy');
  }
}
