import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TestHistoryController } from './test-history.controller';
import { TestHistoryService } from './test-history.service';
import { TestHistoryQuery, TestHistoryQuerySchema } from './schemas/test-history-query.schema';
import { QueriesModule } from '../queries/queries.module';
import { TestHistory, TestHistorySchema } from './schemas/test-history.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: TestHistoryQuery.name, schema: TestHistoryQuerySchema },
      { name: TestHistory.name, schema: TestHistorySchema },
    ]),
    forwardRef(() => QueriesModule),
  ],
  controllers: [TestHistoryController],
  providers: [TestHistoryService],
  exports: [TestHistoryService],
})

export class TestHistoryModule {
}
