import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Param } from '@nestjs/common';
import { TestHistoryService } from './test-history.service';
import { ResponseTestHistoryDto } from '../tests/dtos/response-test-history.dto';

@ApiTags('Test history')
@Controller('test-history')
export class TestHistoryController {
  constructor(
    private testHistoryService: TestHistoryService,
  ) {
  }

  @Get('/:id')
  getTestHistory(@Param() params): Promise<ResponseTestHistoryDto> {
    return this.testHistoryService.getTestHistory(params.id);
  }
}
