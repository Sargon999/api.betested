import { ApiProperty } from '@nestjs/swagger';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { TestDocument } from '../../tests/test.schema';
import { TestHistoryQueryDocument } from './test-history-query.schema';

export type TestHistoryDocument = TestHistory & Document;

@Schema()
export class TestHistory {
  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Test',
  })
  test: TestDocument;

  @ApiProperty()
  @Prop([
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Query',
    },
  ])
  testQueries: Array<TestHistoryQueryDocument>;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Date,
    required: true,
    default: Date.now,
  })
  createdAt: number;
}

export const TestHistorySchema = SchemaFactory.createForClass(TestHistory);