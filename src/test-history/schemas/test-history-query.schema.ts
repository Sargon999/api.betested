import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { Query, QueryDocument } from '../../queries/schemas/queries.schema';
import { TestHistoryDocument } from './test-history.schema';

export type TestHistoryQueryDocument = TestHistoryQuery & Document;

@Schema()
export class TestHistoryQuery {
  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Number,
    required: true,
  })
  statusCode: number;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Number,
    required: true,
  })
  speed: number;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Boolean,
    required: true,
  })
  isEqualResponses: boolean;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Query',
  })
  query: QueryDocument;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Query',
  })
  testHistory: TestHistoryDocument;

  @ApiProperty()
  @Prop({
    type: mongoose.Schema.Types.Mixed,
    required: true,
  })
  jsonResponse: any;
}

export const TestHistoryQuerySchema = SchemaFactory.createForClass(TestHistoryQuery);
