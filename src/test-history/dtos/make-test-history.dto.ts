import { QueryDocument } from '../../queries/schemas/queries.schema';
import { TestDocument } from '../../tests/test.schema';
import { TestHistoryDocument } from '../schemas/test-history.schema';

export interface MakeTestHistoryDto {
  query: QueryDocument,
  loginQuery: QueryDocument,
  test: TestDocument,
  testHistory: TestHistoryDocument,
  targetUrl: string,
}