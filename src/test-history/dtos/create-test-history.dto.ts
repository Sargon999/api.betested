import { Query } from '../../queries/schemas/queries.schema';
import { TestHistory } from '../schemas/test-history.schema';

export interface CreateTestHistoryDto {
  statusCode: number;
  speed: number;
  isEqualResponses: boolean;
  query: Query;
  testHistory: TestHistory;
  jsonResponse: any;
}
