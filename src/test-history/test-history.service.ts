import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { TestHistoryQuery, TestHistoryQueryDocument } from './schemas/test-history-query.schema';
import { CreateTestHistoryDto } from './dtos/create-test-history.dto';
import { MakeTestHistoryDto } from './dtos/make-test-history.dto';
import { constants } from '../common/constants/constants';
import { RequestOptions } from '../tests/types/request-options.interface';
import fetch from 'node-fetch';
import { QueriesService } from '../queries/queries.service';
import { TestHistory, TestHistoryDocument } from './schemas/test-history.schema';
import { ResponseTestHistoryDto } from '../tests/dtos/response-test-history.dto';

@Injectable()
export class TestHistoryService {
  constructor(
    @InjectModel(TestHistoryQuery.name)
    private testHistoryQueryModel: Model<TestHistoryQueryDocument>,
    @InjectModel(TestHistory.name)
    private testHistoryModel: Model<TestHistoryDocument>,
    @Inject(forwardRef(() => QueriesService))
    private queryService: QueriesService,
  ) {
  }

  async createHistoryQueryItem(
    params: CreateTestHistoryDto,
  ): Promise<TestHistoryQueryDocument> {
    const testHistoryItem = new this.testHistoryQueryModel(params);
    return testHistoryItem.save();
  }

  async createTestHistory(testId) {
    const testHistory = new this.testHistoryModel({ test: testId, testQueries: [] });
    return testHistory.save();
  }

  async getTestHistoryByTest(testId): Promise<Array<ResponseTestHistoryDto>> {
    const prepareTestHistory = await this.testHistoryModel
      .find({ test: testId })
      .sort({ createdAt: 'desc' });

    const historyList = [];

    for (const testHistory of prepareTestHistory) {
      const preparedQuery = await this.serializeTestHistory(testHistory);
      historyList.push(preparedQuery);
    }

    return historyList;
  }

  async getTestHistory(testHistoryId): Promise<ResponseTestHistoryDto> {
    const testHistory = await this.testHistoryModel.findById(testHistoryId);
    return await this.serializeTestHistory(testHistory);
  }

  async makeTestHistoryQuery(
    params: MakeTestHistoryDto,
  ): Promise<TestHistoryQueryDocument> {

    const { query, testHistory, targetUrl } = params;
    const originUrl = new URL(targetUrl);

    let token = '';

    if (params?.loginQuery) {
      const { loginQuery } = params;

      const loginData = await this.queryService.getLoginQuery(loginQuery.loginQuery._id);

      if (loginData.isBearerAuth) {
        const authQuery = await fetch(
          `${originUrl.origin}${loginQuery.reqUrl}`,
          this.requestOptions(loginQuery.httpMethod, loginQuery.requestJson),
        );

        const authData = await authQuery.json();

        token = authData[loginData.tokenKey];
      }
    }

    let speed = Date.now();
    const targetQuery = await fetch(
      `${originUrl.origin}${query.reqUrl}`,
      this.requestOptions(query.httpMethod, query.requestJson, token),
    );
    speed = Date.now() - speed;

    const repeatedQuery = await fetch(
      `${originUrl.origin}${query.reqUrl}`,
      this.requestOptions(query.httpMethod, query.requestJson, token),
    );

    const currentQueryObj = await targetQuery.json(),
      repeatedQueryObj = await repeatedQuery.json();

    const objForComparison = this.generateObjForComparison(
      currentQueryObj,
      repeatedQueryObj,
    );

    const isEqualResponses = query?.response ? this.isEqualObjects(
      JSON.parse(query?.response),
      objForComparison,
    ) : false;

    return this.createHistoryQueryItem({
      statusCode: targetQuery.status,
      jsonResponse: JSON.stringify(objForComparison),
      speed,
      isEqualResponses,
      query,
      testHistory,
    });
  }

  private generateObjForComparison(originalObj, similarObj): {} {
    const newObj = Array.isArray(originalObj) ? [] : {};
    for (const key in originalObj) {
      if (typeof originalObj[key] === 'object' && originalObj[key] !== null) {
        const prepareObj = this.generateObjForComparison(
          originalObj[key],
          similarObj[key],
        );
        newObj[key] = Array.isArray(originalObj[key])
          ? Object.values(prepareObj)
          : prepareObj;
      } else {
        if (originalObj[key] !== similarObj[key])
          newObj[key] = constants.IGNORE_VALUE;
        else newObj[key] = originalObj[key];
      }
    }

    return newObj;
  };

  private isEqualObjects(originalObj, newObj): boolean {
    let isEqual = true;

    if (originalObj && newObj && Object.keys(originalObj).length === Object.keys(newObj).length) {
      for (const key in originalObj) {
        if (newObj[key] !== constants.IGNORE_VALUE && originalObj[key] !== null) {
          if (typeof originalObj[key] === 'object') {
            isEqual = this.isEqualObjects(originalObj[key], newObj[key]);
          } else {
            if (originalObj[key] !== newObj[key]) isEqual = false;
          }
        }

        if (!isEqual) break;
      }
    } else {
      isEqual = false;
    }

    return isEqual;
  };

  private requestOptions(method, body, token = ''): RequestOptions {
    const options: RequestOptions = {
      method: method,
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };
    if (body && method !== 'GET') options.body = body;
    return options;
  };


  async serializeTestHistory(testHistory: TestHistoryDocument): Promise<ResponseTestHistoryDto> {
    const prepareQueries: Array<TestHistoryQueryDocument> = [];

    for (const query of testHistory.testQueries) {
      const preparedHistoryQuery = await this.testHistoryQueryModel.findById(query);
      const prepareQuery = await this.queryService.getQuery(preparedHistoryQuery.query);

      Object.assign(preparedHistoryQuery, { query: prepareQuery });

      prepareQueries.push(preparedHistoryQuery);
    }

    return {
      _id: testHistory.id,
      createdAt: testHistory.createdAt,
      testQueries: prepareQueries,
    };
  }
}
