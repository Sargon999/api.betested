import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { config } from 'dotenv';

async function bootstrap() {

  const options = {
    origin: function(origin, callback) {
      if (!origin || [
        'https://betested.ml',
        'http://localhost:3000',
        'http://localhost:3001',
      ].indexOf(origin) !== -1) {
        callback(null, true);
      } else {
        callback(new Error('Not allowed by CORS'));
      }
    },
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    credentials: true,
    sameSite: 'none',
  };

  const app = await NestFactory.create(AppModule);

  app.enableCors(options);
  app.use(cookieParser());

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Proxy')
    .setDescription('The Proxy API')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('api', app, document);

  config();

  app.setGlobalPrefix('v1');
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded());

  await app.listen(5000);
}

bootstrap();
