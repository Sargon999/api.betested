FROM node:16.13.2-alpine
WORKDIR /app
COPY package.json ./
RUN yarn install
COPY . ./
CMD ["yarn", "start:prod"]
