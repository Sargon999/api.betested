module.exports.up = async function(db) {
  await db.createCollection('settings');
  await db.collection('settings').insertOne({
    targetProxyUrl: '',
    ignorePaths: [],
  });
};
